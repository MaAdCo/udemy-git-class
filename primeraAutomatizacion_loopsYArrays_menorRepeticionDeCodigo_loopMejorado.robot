*** Settings ***
Resource    recursosPropios_2.robot
Library     String
Library     Collections

*** Variables ***
${primerNumeroLinkDebajoDeFotoEnHomepage}           1
${ultimoNumeroMasUnoLinkDebajoDeFotoEnHomepage}     8

*** Test Cases ***
C001 Hacer click en contenedores
    SeleniumLibrary.Set Selenium Implicit Wait      10
    
    Abrir browser y esperar logo
    
    :FOR    ${indice}     IN RANGE        ${primerNumeroLinkDebajoDeFotoEnHomepage}       ${ultimoNumeroMasUnoLinkDebajoDeFotoEnHomepage}
    \   Click Element                       xpath=//*[@id="homefeatured"]/li[${indice}]/div/div[2]/h5/a
    \   Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
    \   Click Element                       xpath=${logo_xpath}
    
    Close Browser