*** Settings ***
Resource    recursosPropios_2.robot
Library     String
Library     Collections

*** Variables ***
${numeroLinkDebajoDeFotoEnHomepage}                 1
${nombreEstandarLinkDebajoDeFotosEnHomepage}        //*[@id="homefeatured"]/li[${numeroLinkDebajoDeFotoEnHomepage}]/div/div[2]/h5/a
${nombreLinkInicialDebajoDeFotosEnHomepage}         //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a
${ultimoNumeroMasUnoLinkDebajoDeFotoEnHomepage}      8

*** Test Cases ***
C001 Hacer click en contenedores
    SeleniumLibrary.Set Selenium Implicit Wait      10
    
    Abrir browser y esperar logo
    
    Set Global Variable     @{linksAClickear}       ${nombreLinkInicialDebajoDeFotosEnHomepage}
    
    :FOR    ${indice}    IN RANGE    2   ${ultimoNumeroMasUnoLinkDebajoDeFotoEnHomepage}
    \   ###${numeroLinkDebajoDeFotoEnHomepage}     Set Variable        ${i}
    \   ###Log to console          ${nombreEstandarLinkDebajoDeFotosEnHomepage}
    \   ###/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
    \   ###
    \   ### SI SE DEFINE ${nombreEstandarLinkDebajoDeFotosEnHomepage} DEPENDIENDO 
    \   ### DE ${numeroLinkDebajoDeFotoEnHomepage} EN LA SECCION *** Variables *** 
    \   ### Y SE ACTUALIZA EL VALOR DE LA ÚLTIMA ACÁ,
    \   ### IGUALMENTE NO SE ACTUALIZA EL VALOR DE ${nombreEstandarLinkDebajoDeFotosEnHomepage}
    \   ### PARA GENERAR EL xpath DINÁMICAMENTE
    \   ###
    \   ###\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    \   ${xpathLinkDebajoDeFotosEnHomepage}     Set Variable       //*[@id="homefeatured"]/li[${indice}]/div/div[2]/h5/a
    \   Log to console          ${xpathLinkDebajoDeFotosEnHomepage}
    \   Append To List          ${linksAClickear}       ${xpathLinkDebajoDeFotosEnHomepage}
    
    :FOR    ${link}     IN      @{linksAClickear}
    \   Click Element                       xpath=${link}
    \   Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
    \   Click Element                       xpath=${logo_xpath}
    \   Run Keyword If                      '${link}'=='//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'     Exit For Loop
    
    Close Browser