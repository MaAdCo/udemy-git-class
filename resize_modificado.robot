*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
001 Abrir pagina y cambiar tamano
    Open Browser                        https://www.mobilepractice.io/en/       chrome
    #Set Window Size                     767       600
    #***************************************************************
    #La aparición del menú "hamburguesa" es a < 784x607
    #por dos razones: 
    #   1) Hay una diferencia entre el tamaño con "Set Window Size"
    #       y el real de la ventana...una diferencia de 
    #       14 píxeles en horizontal y 7 píxeles en vertical
    #   2) El cambio ocurre en 770x600 reales
    #***************************************************************
    Set Window Size                     783       607
    Element Should Be Visible           xpath=/html/body/header/div/div/div[2]/div
    #Set Window Size                     768       600
    #***************************************************************
    #La aparición del menú "hamburguesa" es a < 784x607
    #por dos razones: 
    #   1) Hay una diferencia entre el tamaño con "Set Window Size"
    #       y el real de la ventana...una diferencia de 
    #       14 píxeles en horizontal y 7 píxeles en vertical
    #   2) El cambio ocurre en 770x600 reales
    #***************************************************************
    Set Window Size                     784       607
    Element Should Not Be Visible       xpath=/html/body/header/div/div/div[2]/div
    ###Wait Until Element Is Not Visible   xpath=/html/body/header/div/div/div[2]/div
    Close Browser
