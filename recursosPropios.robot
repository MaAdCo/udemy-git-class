*** Settings ***
Documentation       Existe en un documento de texto los pasos manuales
...                 Pruebas mas avanzadas con Robot framework
Library             SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     http://automationpractice.com/index.php
${logo_xpath}   //*[@id="header_logo"]/a/img

*** Keywords ***
Abrir browser y esperar logo
    Open Browser                    ${homepage}     ${browser}
    Wait Until Element Is Visible   xpath=${logo_xpath}
