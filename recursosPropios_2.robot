*** Settings ***
Documentation       Nueva version de recursos propios
...                 Sigo usando Resources para no olvidarme de que son
Library             SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${testUrl}      ${scheme}://${homepage}
${logo_xpath}   //*[@id="header_logo"]/a/img

*** Keywords ***
Abrir browser y esperar logo
    Open Browser                    ${testUrl}     ${browser}
    Wait Until Element Is Visible   xpath=${logo_xpath}
