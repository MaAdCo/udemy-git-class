*** Settings ***
Resource    recursosPropios.robot

*** Variables ***
${seleccion}            OtroQueNoSeaWomen
${lnk_Women_xpath}      //*[@id="block_top_menu"]/ul/li[1]/a
${lnk_Dresses_xpath}    //*[@id="block_top_menu"]/ul/li[2]/a

*** Keywords ***
Seleccionar opcion Women
    Click Element       xpath=${lnk_Women_xpath}
    Title Should Be     Women - My Store

Seleccionar opcion Dresses
    Click Element       xpath=${lnk_Dresses_xpath}
    Title Should Be     Dresses - My Store

*** Test Cases ***
001 Caso con Condicionales
    SeleniumLibrary.Set Selenium Implicit Wait      10
    Abrir browser y esperar logo
    Run Keyword If      '${seleccion}'=='Women'     Seleccionar opcion Women    ELSE    Seleccionar opcion Dresses
    Close Browser