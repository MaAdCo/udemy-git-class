*** Settings ***
Resource    recursos_APIAutomation.robot

*** Variables ***
${getUrlPost}       ${SCHEME}://${HOST}/posts/1

*** Test Cases ***
API001 Obtener Registro de EndPoint
    Success Call    GET     ${getUrlPost}      200
    Response Body Should Contain    userId
    Response Body Should Contain    title
    Response Body Should Contain    body
