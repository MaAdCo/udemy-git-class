*** Settings ***
Documentation       Existe en un documento de texto los pasos manuales
...                 Esta es mi primera automatizacion
Library             SeleniumLibrary

*** Variables ***
${palabraABuscar}   casos de prueba
${navegador}        chrome
${url}              https://www.google.com/

*** Keywords ***
Abrir navegador y esperar logo
    Open Browser                    ${url}     ${navegador}
    Wait Until Element Is Visible   xpath=//*[@id="hplogo"]

*** Test Cases ***
G001 Busqueda de palabras en Google
    SeleniumLibrary.Set Selenium Implicit Wait      10
    Abrir navegador y esperar logo
    Input Text              xpath=//*[@id="tsf"]/div[2]/div/div[1]/div/div[1]/input     ${palabraABuscar}
    Click Element           xpath=//*[@id="tsf"]/div[2]/div/div[2]/div[2]/div/center/input[1]
    Title Should Be         ${palabraABuscar} - Buscar con Google
    Page Should Contain     ${palabraABuscar}
    Close Browser

G002 Hacer click en el boton de busqueda sin escribir palabras en Google
    Abrir navegador y esperar logo
    Click Element           xpath=//*[@id="tsf"]/div[2]/div/div[3]/center/input[1]
    Title Should Be         Google
    Close Browser
