*** Settings ***
Documentation       Existe en un documento de texto los pasos manuales
...                 Esta es mi primera automatizacion
Library             SeleniumLibrary

*** Test Cases ***
G001 Busqueda de palabras en Google
    SeleniumLibrary.Set Selenium Implicit Wait      10
    Open Browser    https://www.google.com/     chrome
    Wait Until Element Is Visible   xpath=//*[@id="hplogo"]
    Input Text      xpath=//*[@id="tsf"]/div[2]/div/div[1]/div/div[1]/input     casos de prueba
    Click Element       xpath=//*[@id="tsf"]/div[2]/div/div[2]/div[2]/div/center/input[1]
    Title Should Be     casos de prueba - Buscar con Google
    Page Should Contain     casos de prueba
    Close Browser

G002 Hacer click en el boton de busqueda sin escribir palabras en Google
    Open Browser    https://www.google.com/     chrome
    Wait Until Element Is Visible   xpath=//*[@id="hplogo"]
    Click Element       xpath=//*[@id="tsf"]/div[2]/div/div[3]/center/input[1]
    Title Should Be     Google
    Close Browser
