*** Settings ***
Resource    recursosPropios_2.robot
Library     String

*** Variables ***
${nombreEstandarLinkDebajoDeFotosEnHomepage}    //*[@id="homefeatured"]/li[${numeroLinkDebajoDeFotoEnHomepage}]/div/div[2]/h5/a
${numeroLinkDebajoDeFotoEnHomepage}             1

*** Keywords ***

*** Test Cases ***
C001 Hacer click en contenedores
    SeleniumLibrary.Set Selenium Implicit Wait      10
    Abrir browser y esperar logo
    Set Global Variable    @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a      //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombreDeContenedor}     IN      @{nombresDeContenedores}
    \   Click Element                       xpath=${nombreDeContenedor}
    \   Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
    \   Click Element                       xpath=${logo_xpath}
    \   Run Keyword If                      '${nombreDeContenedor}'=='//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'     Exit For Loop
    Close Browser