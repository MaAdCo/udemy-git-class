*** Settings ***
Library     SeleniumLibrary
Library     Collections
Library     String

*** Variables ***
${url}                                              http://automationpractice.com/index.php
${navegador}                                        chrome
${seccionPOPULARxPATH}                              //*[@id="home-page-tabs"]/li[1]/a
${closeVentanaOjoXPATH}                             //*[@id="index"]/div[2]/div/div/a
${textoProductoEnVentanaOjoXPATH}                   //*[@id="product"]/div/div/div[2]/h1
${primerIndiceOjo}                                  1
${ultimoIndiceOjoMasUno}                            8
${frameVentanaOjoLuegoDeBorrarIdXPATH}              //*[@id="index"]/div[2]/div/div/div/div/iframe

*** Keywords ***
Abrir navegador y esperar logo
    Open Browser                    ${url}     ${navegador}
    Wait Until Element Is Visible   xpath=//*[@id="header_logo"]/a/img

*** Comments ***
Lo que está acá no se ejecuta...incluso si son keywords válidas.

*** Test Cases ***
AP001 - Verificar que la previsualización está funcionando correctamente a través del icono del ojo.
    SeleniumLibrary.Set Selenium Implicit Wait      10
    Abrir navegador y esperar logo
    #***************************************************************
    #La aparición del ojo es a < 1215x607
    #por dos razones: 
    #   1) Hay una diferencia entre el tamaño con "Set Window Size"
    #       y el real de la ventana...una diferencia de 
    #       14 píxeles en horizontal y 7 píxeles en vertical
    #   2) El cambio ocurre en 1201x600 reales
    #***************************************************************
    Set Window Size     1215     607
    
    #Voy a la sección "POPULAR"
    Set Focus To Element            xpath=${seccionPOPULARxPATH}  
    
    #/////Propuesto por el profesor\\\\\
    Set Selenium Speed      0.2 seconds
    
    :FOR    ${indice}    IN RANGE    ${primerIndiceOjo}     ${ultimoIndiceOjoMasUno} 
    \   ${ojoAClickearXPATH}                Set Variable    //*[@id="homefeatured"]/li[${indice}]/div/div[1]/div/div[1]/a
    \   ${textoProductoHomepageXPATH}       Set Variable    //*[@id="homefeatured"]/li[${indice}]/div/div[2]/h5/a
    \   Log to console                      ${ojoAClickearXPATH} 
    \   Log to console                      ${textoProductoHomepageXPATH}
    \
    \   #/////Propuesto por el profesor\\\\\
    \   Set Focus To Element                xpath=${ojoAClickearXPATH}    
    \
    \   #Obtengo el texto del producto mostrado en la homepage
    \   ${textoProductoEnHomepage}          Get Text        xpath=${textoProductoHomepageXPATH} 
    \
    \   #Clickeo sobre el "ojo"
    \   Click Element                       xpath=${ojoAClickearXPATH} 
    \
    \   #/////Propuesto por el profesor\\\\\
    \   #Solución a "not visible after 5 seconds." usando sólo "Wait Until Element Is Visible       xpath=${textoProductoEnVentanaOjoXPATH}":
    \   #   Hay que borrar el id del frame contenedor y volver a copiar el xpath.
    \   #   Luego hay que "ingresar" al frame con el keyword "Select Frame".
    \   #   Recordar salir del frame cuando se termina de operar en él con "Unselect Frame".
    \   Wait Until Element Is Visible       xpath=${frameVentanaOjoLuegoDeBorrarIdXPATH}
    \   Select Frame                        xpath=${frameVentanaOjoLuegoDeBorrarIdXPATH}                       
    \   
    \   #Verifico que el texto mostrado en la ventana "ojo" se corresponda con el txto mostrado en el homepage
    \   Element Text Should Be              xpath=${textoProductoEnVentanaOjoXPATH}         ${textoProductoEnHomepage}
    \
    \   Unselect Frame
    \
    \   #Cierro la ventana "Ojo"
    \   Click Element                       xpath=${closeVentanaOjoXPATH}
    
    Close Browser
    
AP002 - Verificar que la previsualización está funcionando correctamente a través del texto de preview.
    SeleniumLibrary.Set Selenium Implicit Wait      10
    Abrir navegador y esperar logo
    #***************************************************************
    #La DESaparición del ojo es a < 1216x607
    #por dos razones: 
    #   1) Hay una diferencia entre el tamaño con "Set Window Size"
    #       y el real de la ventana...una diferencia de 
    #       14 píxeles en horizontal y 7 píxeles en vertical
    #   2) El cambio ocurre en 1202x600 reales
    #***************************************************************
    Set Window Size     1216     607
    
    #Voy a la sección "POPULAR"
    Set Focus To Element            xpath=${seccionPOPULARxPATH}  
    
    #/////Propuesto por el profesor\\\\\
    Set Selenium Speed      0.2 seconds
    
    :FOR    ${indice}    IN RANGE    ${primerIndiceOjo}     ${ultimoIndiceOjoMasUno} 
    \   ${ojoAClickearXPATH}                            Set Variable    //*[@id="homefeatured"]/li[${indice}]/div/div[1]/div/div[1]/a
    \   ${textoProductoHomepageXPATH}                   Set Variable    //*[@id="homefeatured"]/li[${indice}]/div/div[2]/h5/a
    \   ${imagenProductoHomepageXPATH}                  Set Variable    //*[@id="homefeatured"]/li[${indice}]/div/div[1]/div/a[1]/img
    \   ${textoQuickPreviewProdcutoHomepageXPATH}       Set Variable    //*[@id="homefeatured"]/li[${indice}]/div/div[1]/div/a[2]
    \   Log to console                      ${ojoAClickearXPATH} 
    \   Log to console                      ${textoProductoHomepageXPATH}
    \   Log to console                      ${imagenProductoHomepageXPATH}
    \   Log to console                      ${textoQuickPreviewProdcutoHomepageXPATH}
    \   
    \   #Verifico que el "ojo" no esté visible
    \   Element Should Not Be Visible          xpath=${ojoAClickearXPATH}
    \
    \   #Obtengo el texto del producto mostrado en la homepage
    \   ${textoProductoEnHomepage}          Get Text        xpath=${textoProductoHomepageXPATH} 
    \
    \   #/////Propuesto por el profesor\\\\\
    \   Mouse Over              xpath=${imagenProductoHomepageXPATH}
    \
    \   #/////Propuesto por el profesor\\\\\
    \   #Prueba durante 2 minutos cada 3 segundos si el keyword pasado como parámetro funcionó
    \   Wait Until Keyword Succeeds          2 min          3 sec           Click Element                       xpath=${textoQuickPreviewProdcutoHomepageXPATH}
    \
    \   #Clickeo sobre el texto "Quick Preview"
    \   ###Click Element                       xpath=${textoQuickPreviewProdcutoHomepageXPATH}
    \
    \   #/////Propuesto por el profesor\\\\\
    \   #Solución a "not visible after 5 seconds." usando sólo "Wait Until Element Is Visible       xpath=${textoProductoEnVentanaOjoXPATH}":
    \   #   Hay que borrar el id del frame contenedor y volver a copiar el xpath.
    \   #   Luego hay que "ingresar" al frame con el keyword "Select Frame".
    \   #   Recordar salir del frame cuando se termina de operar en él con "Unselect Frame".
    \   Wait Until Element Is Visible       xpath=${frameVentanaOjoLuegoDeBorrarIdXPATH}
    \   Select Frame                        xpath=${frameVentanaOjoLuegoDeBorrarIdXPATH}                       
    \   
    \   #Verifico que el texto mostrado en la ventana "ojo" se corresponda con el txto mostrado en el homepage
    \   Element Text Should Be              xpath=${textoProductoEnVentanaOjoXPATH}         ${textoProductoEnHomepage}
    \
    \   Unselect Frame
    \
    \   #Cierro la ventana "Ojo"
    \   Click Element                       xpath=${closeVentanaOjoXPATH}
    
    Close Browser