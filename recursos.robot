*** Settings ***
Documentation       Existe en un documento de texto los pasos manuales
...                 Esta es mi primera automatizacion
Library             SeleniumLibrary

*** Variables ***
${palabraABuscar}   casos de prueba
${navegador}        chrome
${url}              https://www.google.com/

*** Keywords ***
Abrir navegador y esperar logo
    Open Browser                    ${url}     ${navegador}
    Wait Until Element Is Visible   xpath=//*[@id="hplogo"]
